<?php

/*
  Plugin Name: Million Dollar Script Old
  Plugin URI: https://milliondollarscript.com
  Description: A WordPress plugin for integrating with Million Dollar Script 2.1.
  Version: 1.1.1
  Author: Ryan Rhode
  Author URI: https://milliondollarscript.com
  Text Domain: milliondollarscript
  Domain Path: /Languages
  License: GNU/GPL
 */

/**
 * Million Dollar Script Old
 *
 * @version 1.1.1
 * @author Ryan Rhode
 * @copyright (C) 2020, Ryan Rhode
 * @license https://opensource.org/licenses/GPL-3.0 GNU General Public License, version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace MillionDollarScript;

use MillionDollarScript\Classes\Bootstrap;
use MillionDollarScript\Classes\Functions;
use MillionDollarScript\Classes\Options;

defined( 'ABSPATH' ) or exit;

$minimum_version = '7.3.0';
if (version_compare(PHP_VERSION, $minimum_version, '<')) {
	_e('Minimum PHP version requirement not met. Million Dollar Script requires at least PHP ' . $minimum_version, 'milliondollarscript');
	exit;
}

// Define MillionDollarScript base path
define( 'MDS_BASE_FILE', __FILE__ );
define( 'MDS_BASE_PATH', plugin_dir_path( __FILE__ ) );
define( 'MDS_BASE_URL', plugin_dir_url( __FILE__ ) );
define( 'MDS_VENDOR_PATH', MDS_BASE_PATH . 'vendor/' );
define( 'MDS_VENDOR_URL', MDS_BASE_URL . 'vendor/' );

require_once ABSPATH . 'wp-includes/pluggable.php';

require_once MDS_BASE_PATH . 'vendor/autoload.php';

global $mds_bootstrap;
if ( $mds_bootstrap == null ) {
	$mds_bootstrap = new Bootstrap;
}

/**
 * Activation
 */
function milliondollarscript_activate() {
	if ( Options::get_option( 'admin' ) == 'yes' ) {
		Functions::install_mu_plugin();
	}
}

/**
 * Deactivation
 */
function milliondollarscript_deactivate() {
	Functions::delete_mu_plugin();
}

// WP plugin activation actions
register_activation_hook( __FILE__, '\MillionDollarScript\milliondollarscript_activate' );
register_deactivation_hook( __FILE__, '\MillionDollarScript\milliondollarscript_deactivate' );

<?php

/**
 * Million Dollar Script Old
 *
 * @version 1.1.1
 * @author Ryan Rhode
 * @copyright (C) 2020, Ryan Rhode
 * @license https://opensource.org/licenses/GPL-3.0 GNU General Public License, version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace MillionDollarScript\Classes;

defined( 'ABSPATH' ) or exit;

class Database {

	/**
	 * Loads the MDS database global.
	 */
	public static function load() {
		global $mdsdb;

		// don't load it again if it's already loaded
		if ( $mdsdb instanceof \wpdb ) {
			return;
		}

		$MYSQL_USER = Config::get( 'MYSQL_USER' );
		$MYSQL_PASS = Config::get( 'MYSQL_PASS' );
		$MYSQL_DB   = Config::get( 'MYSQL_DB' );
		$MYSQL_HOST = Config::get( 'MYSQL_HOST' );
		$MYSQL_PORT = Config::get( 'MYSQL_PORT' );

		if ( $MYSQL_USER !== null && $MYSQL_PASS !== null && $MYSQL_DB !== null && $MYSQL_HOST !== null && $MYSQL_PORT !== null ) {
			$mdsdb = new \wpdb( $MYSQL_USER, $MYSQL_PASS, $MYSQL_DB, $MYSQL_HOST . ':' . $MYSQL_PORT );
		}
	}
}

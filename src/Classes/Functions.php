<?php

/**
 * Million Dollar Script Old
 *
 * @version 1.1.1
 * @author Ryan Rhode
 * @copyright (C) 2020, Ryan Rhode
 * @license https://opensource.org/licenses/GPL-3.0 GNU General Public License, version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace MillionDollarScript\Classes;

use WPTRT\AdminNotices\Notices;

defined( 'ABSPATH' ) or exit;

class Functions {

	/**
	 * Enqueue scripts and styles
	 */
	public static function enqueue_script() {
		wp_enqueue_style( 'mds-css', MDS_BASE_URL . 'src/Assets/css/mds.css', [], filemtime( MDS_BASE_PATH . 'src/Assets/css/mds.css' ) );

		wp_enqueue_script( 'mds-js', MDS_BASE_URL . 'src/Assets/js/mds.js', [ 'jquery' ], filemtime( MDS_BASE_PATH . 'src/Assets/js/mds.js' ), true );
		wp_localize_script( 'mds-js', 'MDS', [
			'users' => Options::get_option( 'users', 'options', 'no' )
		] );
	}

	/**
	 * Check if allowed to send notices based on DISABLE_NAG_NOTICES define.
	 *
	 * @return bool
	 */
	public static function can_send_notices() {
		return ! defined( 'DISABLE_NAG_NOTICES' ) || ( defined( 'DISABLE_NAG_NOTICES' ) && DISABLE_NAG_NOTICES === false );
	}

	/**
	 * Installs the MU plugin.
	 */
	public static function install_mu_plugin() {
		$wpdomain = parse_url( get_site_url() );

		// create mu-plugins folder if it doesn't exist yet
		if ( ! file_exists( WP_CONTENT_DIR . '/mu-plugins' ) ) {
			mkdir( WP_CONTENT_DIR . '/mu-plugins' );
		}

		$data = "<?php
if ( ! defined( 'MILLIONDOLLARSCRIPT_COOKIES' ) ) {
	define( 'MILLIONDOLLARSCRIPT_COOKIES', true );
} else {
	return;
}
if( ! defined( 'COOKIE_DOMAIN' ) ) {
	define( 'COOKIE_DOMAIN', '.{$wpdomain['host']}' );
}
if( ! defined( 'COOKIEPATH' ) ) {
	define( 'COOKIEPATH', '/' );
}
if( ! defined( 'COOKIEHASH' ) ) {
	define( 'COOKIEHASH', md5( '{$wpdomain['host']}' ) );
}
";
		file_put_contents( WP_CONTENT_DIR . '/mu-plugins/milliondollarscript-cookies.php', $data );
	}

	/**
	 * Displays admin notices related to the path field using WPTRT Admin Notices library.
	 *
	 * @link https://github.com/WPTT/admin-notices
	 */
	public static function path_notices() {
		// Load notices class. Must be above so the Dismiss AJAX can load as well.
		$notices = new Notices();

		// Only show notice on milliondollarscript_options page.
		if ( ( isset( $_GET['page'] ) && $_GET['page'] != 'milliondollarscript_options' ) ) {
			return;
		}

		$options = [
			'scope'         => 'user',
			'screens'       => [],
			'type'          => 'error',
			'alt_style'     => true,
			'capability'    => 'manage_options',
			'option_prefix' => Options::prefix,
		];

		$mds_path = Options::get_mds_path();
		switch ( Options::mds_installed( $mds_path ) ) {
			case "1":
				// Get potential clickable install url
				$install_url     = ( strpos( $mds_path, ABSPATH ) !== false ) ? get_site_url( null, str_replace( ABSPATH, '', $mds_path ) ) : "";
				$install_url_msg = "";
				if ( ! empty( $install_url ) ) {
					$install_url_msg = wp_sprintf( '<a href="%s" target="_blank">%s</a>',
						esc_url( $install_url ),
						esc_html__( 'Click here to install.' )
					);
				}

				// Add notice for folder found but no config.php
				$options['type'] = 'warning';
				$notices->add(
					'mds_path_notices1',
					esc_html__( 'MDS install path', 'milliondollarscript' ),
					esc_html__( 'A folder was found at the specified path but not a config.php. Perhaps you still have to install it.', 'milliondollarscript' ) . $install_url_msg,
					$options
				);
				break;
			case "2":
				// Add notice for folder and config.php found
				$options['type'] = 'success';
				$notices->add(
					'mds_path_notices2',
					esc_html__( 'MDS install path', 'milliondollarscript' ),
					esc_html__( 'A config.php file was found at the specified path. Will try to load Million Dollar Script from here.', 'milliondollarscript' ),
					$options
				);
				break;
			default:
				// Add notice for no folder found
				$notices->add(
					'mds_path_notices0',
					esc_html__( 'MDS install path', 'milliondollarscript' ),
					esc_html__( 'No folder was found at the specified path. You must upload Million Dollar Script and install it before you can use it.', 'milliondollarscript' ),
					$options
				);
				break;
		}

		$notices->boot();
	}

	/**
	 * Delete the MU plugin
	 */
	public static function delete_mu_plugin() {
		if ( file_exists( WP_CONTENT_DIR . '/mu-plugins/milliondollarscript-cookies.php' ) ) {
			unlink( WP_CONTENT_DIR . '/mu-plugins/milliondollarscript-cookies.php' );
		}
	}
}

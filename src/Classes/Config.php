<?php

/**
 * Million Dollar Script Old
 *
 * @version 1.1.1
 * @author Ryan Rhode
 * @copyright (C) 2020, Ryan Rhode
 * @license https://opensource.org/licenses/GPL-3.0 GNU General Public License, version 3
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace MillionDollarScript\Classes;

defined( 'ABSPATH' ) or exit;

class Config {

	/**
	 * Get a value from the MDS config.php file.
	 *
	 * @param $key
	 *
	 * @return mixed|null null if users integration is disabled or config.php wasn't found at the path in the options or value wasn't found.
	 */
	public static function get( $key ) {
		global $mdsdb, $mds_config_lines;

		$path = Options::get_option( 'path', 'options', __DIR__ . '/milliondollarscript' );
		if ( ! $path ) {
			return null;
		}

		// check if config.php exists
		if ( ! isset( $mds_config_lines ) || empty( $mds_config_lines ) ) {

			$mds_config_file = trailingslashit( $path ) . 'config.php';
			if ( ! is_dir( $path ) || ! is_readable( $mds_config_file ) ) {
				return null;
			}

			$mds_config_lines = file( $mds_config_file );
		}

		// parse the value out of the file
		foreach ( $mds_config_lines as $line ) {
			if ( strpos( $line, $key ) !== false ) {

				// get config value for the given key with regex
				preg_match( "/^const\s*{$key}\s*=\s*'?(.+)'?;/", $line, $matches );

				if ( isset( $matches[1] ) ) {
					return trim( stripslashes( $matches[1] ), "'" );
				} else {
					// backwards compatibility
					preg_match( "/^define\(\s*\'{$key}',\s*('.+'|.+)\s*\);/", $line, $matches );

					if ( isset( $matches[1] ) ) {
						return trim( stripslashes( $matches[1] ), "'" );
					}
				}
			}
		}

		if ( ! isset( $mdsdb ) ) {
			exit( __( 'Error retrieving MDS config value for ' . esc_html( $key ) . '.', 'milliondollarscript' ) );
		}

		return $mdsdb->get_var( $mdsdb->prepare( "SELECT `val` FROM `config` WHERE `key` = %s", $key ) );
	}
}


